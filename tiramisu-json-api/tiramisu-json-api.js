/*
@license
Copyright (C) 2019 Team tiramisu (see AUTHORS for all contributors)

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
____________________________________________________________
*/
// Port of tiramisu-json-api (commit: c687960f1517f667d77840616f06da9cd3247a0b)

class ValueOptionError {
  constructor(value, type, path, err) {
    this.value = value;
    this.type = type;
    this.path = path;
    this.err = err;
  }
}

class ValueWarning {
  constructor(value, type, path, err) {
    this.value = value;
    this.type = type;
    this.path = path;
    this.err = err;
  }
}

class Option {
  // fake Option (IntOption, StrOption, ...)
  // only usefull for warnings
  constructor(path) {
    this.path = path;
  }

  impl_getpath() {
    return this.path;
  }
}

class TiramisuOptionOption {
  // config.option(path).option
  constructor(path, schema, model, form) {
    this._path = path;
    this.schema = schema;
    this.model = model;
    this.form = form;
  }

  doc() {
    return this.schema['title'];
  }
  
  path() {
    return this._path;
  }

  name(follow_symlink=false) {
    let path;
    if(!follow_symlink === true ||
        this.isoptiondescription() === true ||
        this.issymlinkoption() === false) {
      path = this._path.split('.');
    } else {
      path = this.schema['opt_path'].split('.')
    }
    return path[path.length - 1];
  }
 
  isoptiondescription() {
    return this.schema['type'] === 'object' || this.schema['type'] === 'array';
  }

  ismasterslaves() {
    return this.schema['type'] === 'array';
  }

  issymlinkoption() {
    return this.schema['type'] === 'symlink'
  }
  
  ismulti() {
    if(this.schema['isMulti'] === undefined) {
      return false;
    }
    return this.schema['isMulti'];
  }

  type() {
      if(this.ismasterslaves() === true) {
        return 'masterslaves';
      }
      if(this.isoptiondescription() === true) {
        return 'optiondescription';
      }
      const types = {'number': 'int',
                     'choice': 'choice',
                     'boolean': 'bool',
                     'password': 'password',
                     'date': 'date',
                     'domain': 'domainname',
                     'url': 'url',
                     'username': 'username',
                     'string': 'str',
                     'symlink': 'symlink'};
      if(this.schema['type'] in types === true) {
        return types[this.schema['type']];
      }
      throw `unsupported type ${this.schema['type']}`;
  }
  
  properties() {
    let props = Array();
    if(this._path in this.model === true) {
      const model = this.model[this._path];
    } else {
      const model = {};
    }
    if(model['required'] === true) {
      props.push('mandatory');
    }
    if(model['readOnly'] === true) {
      props.push('frozen');
    }
    if(this.config.get_hidden(this._path) === true) {
      props.push('hidden');
    }
    if(this._path in this.form && this.form[this._path]['clearable'] === true) {
      props.push('clearable');
    }
    return props;
  }
  
  requires() {
    return null;
  }
}

class TiramisuOptionProperty {
  // config.option(path).property
  constructor(path, model) {
    this.path = path
    this.model = model;
  }

  get(only_raises = false) {
    let properties;
    if(only_raises === false) {
      if(this.model['properties']) {
        properties = this.model['properties'].slice();
      } else {
        properties = Array();
      }
      if(this.model['required'] === true) {
        properties.push('mandatory');
      }
      if(this.model['readOnly'] === true) {
        properties.append('frozen');
      }
    } else {
        properties = Array();
    }
    if(this.config.get_hidden(this.path) === true) {
      properties.push('hidden');
    }
    return properties;
  }
}

class _Value {
  _dict_walk(warns, ret, schema, root, fullpath, withwarning) {
    for(const key of Object.keys(schema['properties'])) {
      const option = schema['properties'][key];
      let hidden;
      let model_hidden;
      if(this.temp[key] !== undefined && 'hidden' in this.temp[key] === true) {
        hidden = this.temp[key]['hidden'];
      } else {
        hidden = null;
      }
      if(this.model[key] !== undefined &&
          this.model[key]['hidden'] !== true && 
          this.model[key]['display'] !== false) {
        model_hidden = true;
      } else {
        model_hidden = false;
      }

      if(hidden === false || (hidden === null && model_hidden)) {
        if(option['type'] === 'object') {
          this._dict_walk(warns,
                          ret,
                          option,
                          root,
                          fullpath,
                          withwarning);
        } else {
          const value = this.config.get_value(key, schema);
          this._display_warnings(warns, key, value, option['type'], option['name'], withwarning)
          ret[key] = value
        }
      }
    }
  }

  dict(fullpath=false, withwarning=false, warns=null) {
    let ret = {};
    this._dict_walk(warns,
                    ret,
                    this.schema,
                    this.path,
                    fullpath,
                    withwarning);
    return ret;
  }

  _display_warnings(warns, path, value, type, name, withwarning=true) {
    if(warns !== null) {
      if(path in this.model === true) {
        if('error' in this.model[path] === true) {
          for(const err of this.model[path]['error']) {
            warns.push(new ValueOptionError(value, type, path, err));
          }
        }
        if('warnings' in this.model[path] === true) {
          for(const err of this.model[path]['warnings']) {
            warns.push(new ValueWarning(value, type, path, err));
          }
        }
      }
    }
  }
}

class TiramisuOptionOwner {
  // config.option(path).owner
  constructor(config, schema, model, form, temp, path, index) {
    this.config = config;
    this.schema = schema;
    this.model = model;
    this.form = form;
    this.temp = temp;
    this.path = path;
    this.index = index;
  }

  isdefault() {
    return this.config.get_owner(this.path) === 'default'
  }
}

class TiramisuOptionValue extends _Value {
  // config.option(path).value
  constructor(config, schema, model, form, temp, path, index) {
    super();
    this.config = config;
    this.schema = schema;
    this.model = model;
    this.form = form;
    this.temp = temp;
    this.path = path;
    this.index = index;
  }

  get(warns=null) {
    const value = this.config.get_value(this.path, this.schema);
    this._display_warnings(warns, this.path, value, this.schema['type'], this.schema['name']);
    return value;
  }

  list() {
    return this.schema['enum'];
  }

  _validate(type_, value) {
    //FIXME undefined ?
    if(value === null) {
      return
    }
    if(type_ === 'choice') {
      if(this.schema['enum'].indexOf(value) === -1) {
        throw `value ${value} is not a in ${this.schema['enum']}`;
      }
    } else if(!typeof value === type_) {
      throw `value ${value} is not a valid ${type_}`;
    }
  }

  set(value, warns=null) {
    const type_ = this.schema['type'];
    let remote;
    if(this.schema['isMulti'] === true) {
      if(!typeof value === 'list') {
        throw `value must be a list`;
      }
      for(const val of value) {
        this._validate(type_, val);
      }
    } else {
      this._validate(type_, value);
    }
    if(this.path in this.form === true && this.form[this.path]['remote'] === true) {
      remote = true;
    } else {
      remote = false;
    }
    this.config.modify_value(this.path, this.index, value, remote);
    this._display_warnings(warns, this.path, value, type_, this.schema['name']);
  }

  reset() {
    if(this.path in this.form === true && this.form[this.path]['remote'] === true) {
      const remote = true;
    } else {
      const remote = false;
    }
    this.config.delete_value(this.path, this.index, remote);
  }
}

class _Option {
  constructor() {
    this.list = function(type='option') {
      if(type in ['all', 'option'] === true) {
        throw `not implemented`;
      }
      let ret = Array();
      let schema;
      if('properties' in this.schema === true) {
        schema = this.schema['properties'];
      } else {
        schema = this.schema;
      };
      for(const path of Object.keys(schema)) {
        const subschema = schema[path];
        if(type === 'all' || subschema['type'] in ['object', 'array'] === false) {
          let hidden;
          let model_hidden;
          if(this.temp[path] !== undefined && 'hidden' in this.temp[path] === true) {
            hidden = this.temp[path]['hidden'];
          } else {
            hidden = null;
          }
          if(this.model[path] !== undefined &&
              this.model[path]['hidden'] !== true && 
              this.model[path]['display'] !== false) {
            model_hidden = true;
          } else {
            model_hidden = false;
          }

          if(hidden === false || (hidden === null && model_hidden)) {
            if(['object', 'array'].indexOf(subschema['type']) !== -1) {
              ret.push(new TiramisuOptionDescription(this.config, subschema, this.model, this.form, this.temp, path));
            } else {
              ret.push(new TiramisuOption(this.config, subschema, this.model, this.form, this.temp, path, this.index));
            }
          }
        }
      }
      return ret;
    }
  }
}

class TiramisuOptionDescription extends _Option {
  // config.option(path) (with path == OptionDescription)
  constructor(config, schema, model, form, temp, path) {
    super();
    this.config = config;
    this.schema = schema;
    this.model = model;
    this.form = form;
    this.temp = temp;
    this.path = path;
    this.index = null;
    this.option = new TiramisuOptionOption(this.path, this.schema, this.model, this.form);
    let p_model;
    if(this.path in this.model === true) {
      p_model = this.model[this.path];
    } else {
      p_model = {};
    }
    this.property = new TiramisuOptionProperty(this.path, p_model);
    this.value = new TiramisuOptionValue(this.config, this.schema, this.model, this.form, this.temp, this.path, this.index);
  }

  group_type() {
    if(this.temp[key] !== undefined && 'hidden' in this.temp[key] === true) {
      const hidden = this.temp[key]['hidden'];
    } else {
      const hidden = null;
    }
    if(this.model[key] !== undefined &&
        this.model[key]['hidden'] !== true && 
        this.model[key]['display'] !== false) {
      const model_hidden = true;
    } else {
      const model_hidden = false;
    }

    if(hidden === false || (hidden === null && model_hidden)) {
      return 'default';
    }
    // FIXME
    throw `PropertiesOptionError`;
  }
}

class TiramisuOption {
  // config.option(path) (with path == Option)
  constructor(config, schema, model, form, temp, path, index) {
    this.config = config;
    this.schema = schema;
    this.model = model;
    this.form = form;
    this.temp = temp;
    this.path = path;
    this.index = index;
    if(this.index !== null) {
      throw `not implemented`;
    }
    this.option = new TiramisuOptionOption(this.path, this.schema, this.model, this.form);
    this.value = new TiramisuOptionValue(this.config, this.schema, this.model, this.form, this.temp, this.path, this.index);
    this.owner = new TiramisuOptionOwner(this.config, this.schema, this.model, this.form, this.temp, this.path, this.index);
    let p_model;
    if(this.path in this.model === true) {
      p_model = this.model[this.path];
    } else {
      p_model = {};
    }
    this.property = new TiramisuOptionProperty(this.path, p_model);
  }
}

class TiramisuContextProperty {
  get() {
    return ['demoting_error_warning'];
  }
}

//class _ContextOption extends _Option {
//  constructor(config, model, form, schema, temp) {
//    super();
//    this.config = config;
//    this.schema = {'properties': schema};
//    this.model = model;
//    this.form = form;
//    this.temp = temp;
//  }
//}
//
////class ContextOption extends _Option {
//class ContextOption {
//  // config.option
//  constructor(config, model, form, schema, temp) {
//    this._call = new _ContextOption(config, model, form, schema, temp)
//    for(const val of this._call.valueOf()) {
//      this[val] = this._call[val];
//    }
//  }
//}

class ContextValue extends _Value {
  // config.value
  constructor(config, model, form, schema, temp) {
    super();
    this.config = config;
    this.schema = {'properties': schema};
    this.model = model;
    this.form = form;
    const first = Object.keys(schema)[0];
    this.path = first.split('.').slice(0, -1).join('.');
    this.temp = temp;
  }

  //FIXME CALL

  mandatory() {
    const dict = this.dict();
    let ret = Array();
    for(const key of Object.keys(dict)) {
      const value = dict[key];
      if(this.model[key] && this.model[key]['required'] === true
            && value === null ||
            (this.schema['isMulti'] === true && (null in value === true || '' in value === true))) {
        ret.push(key);
      }
    }
    return ret;
  }
}

class Config {
  // config
  constructor(json) {
    this.model_ori = json['model'];
    this.model = {};
    for(const option of json['model']) {
      this.model[option['key']] = option;
    }
    this.form = {};
    for(const option of json['form']) {
      if('key' in option === true) {
        //FIXME pattern!
        this.form[option['key']] = option;
      }
    }
    this.temp = {};
    this.schema = json['schema'];
    this.updates = Array();
    //for _Option
    this.index = null;
    this.config = this;
    const first_path = Object.keys(this.schema)[0];
    if(first_path.indexOf('.') >=0) {
      //FIXME
      this.root = first_path.split('.').slice(0, -1).join('.');
    } else {
      this.root = '';
    }
    this.property = new TiramisuContextProperty();

    //FIXME new ContextOption(this, this.model, this.form, this.schema, this.temp);
    this.option = function(path, index=null) {
      const schema = this.get_schema(path);
      if(['object', 'array'].indexOf(schema['type']) !== -1) {
        return new TiramisuOptionDescription(this, schema, this.model, this.form, this.temp, path);
      } else {
        return new TiramisuOption(this, schema, this.model, this.form, this.temp, path, index);
      }
    };
    const _call = new _Option();
    for(const val of Object.keys(_call.valueOf())) {
      this.option[val] = _call[val].bind(this);
    }
    this.value = new ContextValue(this, this.model, this.form, this.schema, this.temp);
  }

  add_value(path, index, value, remote) {
    this.updates_value('add', path, index, value, remote, null);
  }

  modify_value(path, index, value, remote) {
    const schema = this.get_schema(path);
    if(value !== null && Array.isArray(value) === true && value[len(value)] === undefined) {
      const new_value = schema['defaultvalue'] ? schema['defaultvalue'] : null;
      if(new_value === null) {
        const len_value = value.length;
        const schema_value = schema['value'] ? schema['value'] : Array();
        if(schema_value.length >= len_value) {
          new_value = schema_value[len_value - 1];
        }
      }
      value[-1] = new_value;
    }
    this.updates_value('modify', path, index, value, remote, null);
  }

  delete_value(path, index, remote) {
    const schema = this.get_schema[path];
    const value = schema['value'] ? schema['value'] : null;
    if(value === null && schema['isMulti'] === true) {
      value = Array();
    }
    this.updates_value('delete', path, index, value, remote, null);
  }

  get_schema(path) {
    let root_path = this.root;
    let schema = {'properties': this.schema, 'type': 'object'};
    let root;
    let subpaths;
    if(root_path !== '') {
      root = this.root.split('.');
      if(!path.startsWith(root)) {
        throw `cannot find ${path}`
      }
      subpaths = path.split('.').slice(root.length);
    } else {
      subpaths = path.split('.');
    }
    for(const subpath of subpaths) {
      if(root_path !== '') {
        root_path += '.' + subpath;
      } else {
        root_path = subpath;
      }
      schema = schema['properties'][root_path];
    }
    return schema;
  }

  get_hidden(path) {
    const property = 'hidden';
    let value;
    if(this.temp && path in this.temp === true && property in this.temp[path] === true) {
      value = this.temp[path][property];
    } else if(this.model && path in this.model === true && property in this.model[path] === true) {
      value = this.model[path][property];
    }
    return value
  }

  get_value(path, schema) {
    let value;
    if(this.temp && path in this.temp === true && 'value' in this.temp[path] === true) {
      value = this.temp[path]['value'];
    } else if(this.model && path in this.model === true && 'value' in this.model[path] === true) {
      value = this.model[path]['value'];
    } else if(schema['isMulti'] === true) {
      value = Array();
    } else {
      value = null;
    }
    return value
  }

  get_owner(path) {
    let owner;
    if(this.temp && path in this.temp === true && 'owner' in this.temp[path] === true) {
      owner = this.temp[path]['owner'];
    } else {
      if(this.model && path in this.model === true && 'owner' in this.model[path] === true) {
        owner = this.model[path]['owner'];
      } else {
        owner = 'default';
      }
    }
    return owner
  }

  updates_value(action, path, index, value, remote, masterslaves) {
    let update_last_action = false;
    let last_body;
    if(this.updates.length !== 0) {
      last_body = this.updates[this.updates.length - 1];
      if(last_body['name'] == path) {
        if(index === null && 'index' in last_body === false) {
          const last_action = last_body['action'];
          if(last_action === action ||
              last_action in ['delete', 'modify'] === true && action in ['delete', 'modify'] === true) {
            update_last_action = true;
          }
        } else if(index === null && action === 'delete') {
          for(const update of this.updates.reverse()) {
            if(masterslaves === null && udpate['name'] === path ||
                masterslaves && path.startsWith(masterslaves + '.')) {
              this.updates = this.updates.slice(0, -1);
            } else {
              break;
            }
          }
        } else if(last_body['index'] === index) {
          if(last_body['action'] === 'add' && action === 'modify') {
            action = 'add';
            update_last_action = true;
          } else if(last_body['action'] === action && action !== 'delete' ||
                    last_body['action'] === 'modify' && action === 'delete') {
            update_last_action = true;
          } else if(last_body['action'] === 'add' && action === 'delete') {
            this.updates = this.updates.slice(0, -1);
          }
        }
      }
    }
    if(update_last_action === true) {
      if(value === null) {
        if('value' in last_body === true) {
          last_body['value'].slice(0, -1);
        }
      } else {
        last_body['value'] = value;
      }
      if(index === null && 'index' in last_body === true) {
        last_body['value'].slice(0, -1);
      }
      last_body['action'] = action;
    } else {
      let data = {'action': action, 'name': path};
      if(value !== null) {
        data['value'] = value;
      }
      if(index !== null) {
        data['index'] = index;
      }
      this.updates.push(data);
    }
    let match;
    if('pattern' in this.form[path] === true && (Array.isArray(value) === false || ! undefined in value === true)) {
      match = this.test_value(path, value, remote);
    } else {
      match = true;
    }
    if(match === true) {
      if(remote === true) {
        const ret = this.send_data({'updates': this.updates, 'model': this.model_ori});
        this.updates = Array();
        for(const key of Object.keys(this.temp)) {
          delete this.temp[key];
        };
      } else {
        if(path in this.temp === false) {
          this.temp[path] = {};
        }
        const old_value = this.get_value(path, this.get_schema(path));
        const old_owner = this.get_owner(path);
        this.temp[path]['owner'] = 'tmp';
        this.temp[path]['value'] = value;
        let changes = {};
        changes[path] = {};
        if(old_value !== value) {
          changes[path]['value'] = value;
        }
        if(old_owner !== this.temp[path]['owner']) {
          changes[path]['owner'] = this.temp[path]['owner'];
        }
        this.set_dependencies(path, value, undefined, changes);
        this.set_not_equal(path, value);
        this.do_copy(path, value, changes);
        for(const change_path of Object.keys(changes)) {
          if(Object.keys(changes[change_path]) !== 0) {
            document.dispatchEvent(new CustomEvent('tiramisu_change',
                                                   {'detail': {'path': change_path, 'change': changes[change_path]}}));
          }
        }
      }
    }
  }

  test_value(path, value, remote) {
    if(Array.isArray(value) === true) {
      for(const val of value) {
        if(!this.test_value(path, val, remote)) {
          return false;
        }
        return true;
      }
    } else {
      let match;
      if(value === null) {
        match = true;
      } else {
        //FIXME !!!
        match = true;
        //return this.form[path]['pattern'];
      }
      if(remote === false) {
        if(match === false) {
          if(path in this.temp === false) {
            this.temp[path] = {};
          }
          this.temp[path]['error'] = [''];
        } else if('error' in this.model[path] === true) {
          delete this.temp[path]['error'];
        }
      }
      return match;
    }
  }

  set_dependencies(path, ori_value, force_hide, changes) {
    let dependencies;
    if(path in this.form === true) {
      dependencies = this.form[path]['dependencies'] ? this.form[path]['dependencies'] : {};
    } else {
      dependencies = {};
    }
    if(Object.keys(dependencies).length != 0) {
      let expected;
      if(ori_value in dependencies['expected'] === true) {
        expected = dependencies['expected'][ori_value];
      } else {
        expected = dependencies['default'];
      }
      for(const action of ['hide', 'show']) {
        const expected_actions = expected[action];
        if(expected_actions !== undefined) {
          let hidden;
          if(force_hide === true) {
            hidden = true;
          } else {
            hidden = action == 'hide';
          }
          for(const expected_path of expected_actions) {
            if(expected_path in this.temp === false) {
              this.temp[expected_path] = {};
            }
            const old_hidden = this.get_hidden(expected_path);
            this.temp[expected_path]['hidden'] = hidden;
            if(old_hidden !== hidden) {
              if(expected_path in changes === false) {
                changes[expected_path] = {};
              }
              changes[expected_path]['hidden'] = hidden;
            }
            let value;
            if('value' in this.temp[expected_path] === true) {
              value = this.temp[expected_path]['value'];
            } else {
              value = this.model[expected_path]['value'] ? this.model[expected_path]['value'] : null;
            }
            this.set_dependencies(expected_path, value, hidden, changes);
          }
        }
      }
    }
  }

  set_not_equal(path, value) {
    const not_equal = this.form[path]['not_equal'] ? this.form[path]['not_equal'] : {};
    if(not_equal.length !== 0) {
      let vals = Array();
      let opts = Array();
      if(Array.isArray(value) === true) {
        for(const val of value) {
          vals.push(val);
          opts.push(path);
        }
      } else {
        vals.push(value);
        opts.push(path);
      }
      if(this.form[path]['not_equal'] !== undefined) {
        for(const path_ of this.form[path]['not_equal']['options']) {
          const schema = this.get_schema(path_);
          const p_value = this.get_value(path_, schema);
          if(Array.isArray(p_value) === true) {
            for(const val of p_value) {
              vals.push(val)
              opts.push(path_)
            }
          } else {
            vals.push(value)
            opts.push(path_)
          }
        }
      }
      let equal = Array();
      let warnings_only;
      if(this.form[path]['not_equal'] !== undefined) {
        warnings_only = this.form[path]['not_equal']['warnings'] ? this.form[path]['not_equal']['warnings'] : false;
      } else {
        warnings_only = false;
      }
      let idx_inf = -1;
      for(const val_inf of vals) {
        idx_inf += 1;
        let idx_sup = -1;
        for(const val_sup of vals.slice(idx_inf + 1)) {
          idx_sup += 1;
          if(val_inf !== null && val_inf === val_sup) {
            for(const opt_ of [opts[idx_inf], opts[idx_inf + idx_sup + 1]]) {
              if (opt_ in equal === false) {
                equal.push(opt_);
              }
            }
          }
        }
      }
      if(equal.length !== 0) {
        let equal_name = {};
        for(const opt of equal) {
          const schema = this.get_schema(opt);
          equal_name[opt] = schema['title'];
        }
        for(const opt_ of equal) {
          let display_equal = Array();
          for(const opt__ in equal) {
            if(opt_ != opt__) {
              display_equal.push(equal_name[opt_]);
            }
          }
          display_equal = display_equal.join(', ');
          if(opt_ === path) {
            if(warnings_only !== false) {
              if('warning' in this.model[opt_] === false) {
                this.model[opt_]['warnings'] = Array();
              }
              this.model[opt_]['warnings'].push(`value for ${display_equal[opt_]} should be different`);
            } else {
              if('error' in this.model[opt_] === false) {
                this.model[opt_]['error'] = Array();
              }
              this.model[opt_]['error'].push(`value for ${display_equal[opt_]} must be different`);
            }
          } else {
            if(warnings_only !== false) {
              if(opt_ in this.model === false) {
                this.model[opt_] = {};
              }
              if('warnings' in this.model[opt_] === false) {
                this.model[opt_]['warnings'] = Array();
              }
              this.model[opt_]['warnings'].push(`should be different from the value of "${display_equal[opt_]}"`);
            } else {
              if(opt_ in this.model === false) {
                this.model[opt_] = {};
              }
              if('error' in this.model[opt_] === false) {
                this.model[opt_]['error'] = Array();
              }
              this.model[opt_]['error'].push(`must be different from the value of "${display_equal[opt_]}"`);
            }
          }
        }
      } else {
        for(const opt of opts) {
          if('warning' in this.model[opt] === true) {
            delete this.model[opt]['warnings'];
          }
          if('error' in this.model[opt] === true) {
            delete this.model[opt]['error'];
          }
        }
      }
    }
  }

  do_copy(path, value, changes) {
    const copy = this.form[path]['copy'] ? this.form[path]['copy'] : {};
    if(Object.keys(copy).length !== 0) {
      for(const opt of copy) {
        const owner = this.get_owner(opt);
        if(owner === 'default') {
          // do not change in this.temp, it's default value
          if(this.model[opt]['value'] !== value) {
            this.model[opt]['value'] = value;
            if(opt in changes === false) {
              changes[opt] = {}
            }
            changes[opt]['value'] = value;
          }
        }
      }
    }
  }

  send_data(updates) {
    throw 'please implement send_data function';
  }
}

export { Config };
//module.exports = Config;
